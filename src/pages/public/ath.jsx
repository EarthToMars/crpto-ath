import React, { useEffect, useState } from "react";
import axios from "axios";
import Select from "react-select";
const ATH = () => {
  const [allTokens, setAllTokens] = useState([]);
  const [selectedTokens, setSelectedTokens] = useState([]);
  useEffect(() => {
    axios
      .get(
        `${process.env.REACT_APP_NOMICS_API_URL}currencies/ticker?key=${process.env.REACT_APP_NOMICS_API_KEY}&ids=POOLZ,URQA,FRM,FRMX`
      )
      .then((response) => {
        setAllTokens(response.data);
        setSelectedTokens(
          [].concat(
            ...response.data.map((token) => {
              return { value: token.symbol, label: token.symbol };
            })
          )
        );
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  const onSelect = (data) => {
    setSelectedTokens([...data]);
  };

  return (
    <>
      <h2 className="mb-5">CRYPTO ATH</h2>
      <Select
        value={selectedTokens}
        isMulti
        name="colors"
        options={[].concat(
          ...allTokens.map((token) => {
            return { value: token.symbol, label: token.symbol };
          })
        )}
        className="basic-multi-select"
        classNamePrefix="select"
        placeholder="Select Token"
        onChange={onSelect}
      />
      <div className="row mt-5">
        <div className="col-lg-6">TOKEN</div>
        <div className="col-lg-6">ATH</div>
      </div>
      {allTokens
        .filter((filterItem) => {
          const el = selectedTokens.find(
            (searchItem) => searchItem.value === filterItem.symbol
          );
          if (el) {
            return filterItem;
          } else {
            return null;
          }
        })
        .map((token) => (
          <div className="row">
            <div className="col-lg-6">{token.symbol}</div>
            <div className="col-lg-6">{token.high ? token.high : "NA"}</div>
          </div>
        ))}
      <a className="float-right mt-5" href="https://nomics.com" target="_self">
        Crypto Market Cap & Pricing Data Provided By Nomics.
      </a>{" "}
    </>
  );
};

export default ATH;
