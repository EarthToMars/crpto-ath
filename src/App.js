import React from "react";
import "./App.css";
import ATH from "./pages/public/ath";
function App() {
  return (
    <div className="container p-5">
      <ATH />
    </div>
  );
}

export default App;
